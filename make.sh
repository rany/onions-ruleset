#!/usr/bin/env bash

set -f -e

main_script() {
	rdest="$(pwd)"
	[ -d "/tmp/" ] && rdest="/tmp/"
	[ -d "/dev/shm/" ] && rdest="/dev/shm/"
	dest="$(mktemp -d -t clearnet-to-onion-ruleset_XXXXXXX -p "$rdest")"
	echo " * Using $dest as temporary directory"

	[ ! -f key.pem ] && openssl genrsa -out key.pem 4092 && echo " * Created private key $(pwd)/key.pem"
	[ ! -f public.pem ] && openssl rsa -in key.pem -outform PEM -pubout -out public.pem && echo " * Created public key $(pwd)/public.pem"
	if [ ! -f jwk.object ]; then
		echo " * Installing pem-jwk via NPM" && npm install pem-jwk && echo " * Installed pem-jwk via NPM"
		node_modules/.bin/pem-jwk < public.pem > jwk.object && echo " * Created jwk.object"
	fi

	num_of_rules_created=0
	echo " * Creating rules..."
	while read -r in; do
		if [[ ! "$in" =~ ^#.*|^$|^\<!--.*$ ]]; then
			make_rule "$dest" $in &
			num_of_rules_created=$((num_of_rules_created + 1))
		fi
	done < onions.md
	wait
	echo -n " * Created $num_of_rules_created rule"
	[[ $num_of_rules_created == 0 ]] || [[ $num_of_rules_created -gt "1" ]] && echo -n "s"
	echo "."

	python3 https-everywhere/merge-rulesets.py --source_dir "$dest/" && echo " * Merged all rules to one ruleset file."
	sign_ruleset "$dest/default.rulesets" key.pem public/
	echo " * Ruleset signed and copied to public."
	[ ! -f public/jwk.object ] && cp jwk.object public/ && echo " * jwk.object copied to public."
	rm -r "${dest:?}"
	echo " * Temporary directory deleted."
	echo " * Took ${SECONDS}s."
}

make_rule() {
	if [ -z "$3" ]; then
		echo "syntax: $0 dest_dir clearnet_addr_without_proto onion_addr_with_proto target_host(optional)" >&2
		exit 1
	fi
	f="$2"
	s="$(basename "$3")"
	sp="$(echo "$3" | sed -e's,^\(.*://\).*,\1,g')" #second protocol

	capitalized="${f^}"
	dest="$1/$capitalized.xml"

	if [ -z "$4" ]; then
		lower="${f,,}"
	else
		lower="$4"
	fi

	{
		printf %s "<ruleset name=\"""$capitalized""\">"
		printf %s "<target host=\"""$lower""\" />"
		printf %s "<rule from=\""^http[s]?://"$f""\" to=\"""$sp""$s""\" />"
		printf %s "</ruleset>"
	} > "$dest"
}

sign_ruleset() {
	if [ $# -ne 3 ]; then
		echo "Usage: $0 source_rules private_key_file output_path"
		exit 1
	fi

	RULESETS_FILE="$1"
	mkdir -p "$3"
	TIMESTAMP=$(date +%s)
	echo "{\"timestamp\":$TIMESTAMP,\"rulesets\":$(<"$RULESETS_FILE")}" \
		| gzip -nc > "$3/default.rulesets.$TIMESTAMP.gz"

	openssl dgst -sha256 \
		-sigopt rsa_padding_mode:pss -sigopt rsa_pss_saltlen:32 \
		-sign "$2" -out "$3/rulesets-signature.$TIMESTAMP.sha256" \
		"$3/default.rulesets.$TIMESTAMP.gz"

	echo "$TIMESTAMP" > "$3/latest-rulesets-timestamp"
}

main_script
