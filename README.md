# onions-ruleset

path prefix:   
`https://codeberg.org/rany/onions-ruleset/raw/branch/master/public/`

jwk:    
`{"kty":"RSA","n":"C6yYaOr5Y563Eta_UbJDjPkuNC66fOpAyahbXEIYvfTwYmBJSR_klluGvkU0hc8Wm1bi1Nba8EkM6W7CQZ51o9ZWr7UVdVTGUpanRHpS0jHl2IWpSLw4W_oRniICSVv8x_pkFzjsH_5-jxHlqndCnfcBmc9oOed9YGyN1p97GRuAcVPpk3MuGdoM8Izts2eTnC87vALSuHaQUedr34eUEPN3SPRT3BkSU0F7reeFFEemHzBS_4XMkQMF-67kyRVu-Uy9LDDHfIkISHC3YXjA_zee5_4uP5qKqkxGvshqAXssSdZmZF1kgc42IXJRFd1tlOsAyo3dKaB60qC9AtSJDHbbcw0KijTibNaFw5ZdrC9jHKSNRziIbTD4C_C5hrYbuKwOC2Nr-X01bmAFTlua3ijEFggRK5Ev4J8XGV37W9eLFA2djZBUPMgDF2g4xHHsRHbkZdO22wYBqLxy_WQRzoPVNz_r2sZZ5WMSQopwWZiHU7_QgyTsD_5sTH-yzewPlBAChT5n6x2WCf5-2K6NuPpYrWghw7lnaMiM19WpQE71RgoXnitbTGcyAFkIyua6ejCkRQICHxVmo7Sr_y8TzzBeBfr1OMUY0EL57puSkPZkLdw_dkWsX3YSpuC1ywAW2e0cHxWEQQgpyw0thE1DFvsscJua5jAXVorOadEOfas","e":"AQAB"}`

## How to add onions?

modify `onions.md` and send pull requests

## How to make?

### Dependencies

* git 
* npm
* mktemp
* sed
* openssl
* minimum python3.6
* minimum bash4.0

### Pull sources

```
git clone https://codeberg.org/rany/onions-ruleset.git
cd onions-ruleset
```

### Create the ruleset

```
./make.sh
```

### Serve the ruleset

now you can serve everything in `public/` so that people can use your ruleset
